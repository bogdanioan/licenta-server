export const cppKeywords = {
    aliognas: 'alignas',
    alignof: 'alignof',
    and: 'and',
    and_eq: 'and_eq',
    asm: 'asm',
    atomic_cancel: 'atomic_cancel',
    atomic_commit: 'atomic_commit',
    atomic_noexcept: 'atomic_noexcept',
    auto: 'auto',
    bitand: 'bitand',
    bitor: 'bitor',
    bool: 'bool',
    break: 'break',
    case: 'case',
    catch: 'catch',
    char: 'char',
    char8_t: 'char8_t',
    char16_t: 'char16_t',
    char32_t: 'char32_t',
    class: 'class',
    compl: 'compl',
    concept: 'concept',
    // const: 'const',
    cout:'cout',
    cin:'cin',
    consteval: 'consteval',
    constexpr: 'constexpr',
    const_cast: 'const_cast',
    continue: 'continue',
    co_await: 'co_await',
    co_return: 'co_return',
    co_yield: 'co_yield',
    decltype: 'decltype',
    default: 'default',
    delete: 'delete',
    do: 'do',
    double: 'double',
    dynamic_cast: 'dynamic_cast',
    else: 'else',
    enum: 'enum',
    explicit: 'explicit',
    export: 'export',
    extern: 'extern',
    false: 'false',
    float: 'float',
    for: 'for',
    friend: 'friend',
    goto: 'goto',
    if: 'if',
    include: '#include',
    inline: 'inline',
    int: 'int',
    long: 'long',
    mutable: 'mutable',
    namespace: 'namespace',
    new: 'new',
    noexcept: 'noexcept',
    not: 'not',
    not_eq: 'not_eq',
    nullptr: 'nullptr',
    operator: 'operator',
    or: 'or',
    or_eq: 'or_eq',
    private: 'private',
    protected: 'protected',
    public: 'public',
    reflexpr: 'reflexpr',
    register: 'register',
    reinterpret_cast: 'reinterpret_cast',
    requires: 'requires',
    return: 'return',
    short: 'short',
    signed: 'signed',
    sizeof: 'sizeof',
    static: 'static',
    static_assert: 'static_assert',
    static_cast: 'static_cast',
    struct: 'struct',
    switch: 'switch',
    synchronized: 'synchronized',
    template: 'template',
    this: 'this',
    thread_local: 'thread_local',
    throw: 'throw',
    true: 'true',
    try: 'try',
    typedef: 'typedef',
    typeid: 'typeid',
    typename: 'typename',
    union: 'union',
    unsigned: 'unsigned',
    using: 'using',
    virtual: 'virtual',
    void: 'void',
    volatile: 'volatile',
    wchar_t: 'wchar_t',
    while: 'while',
    xor: 'xor',
    xor_eq: 'xor_eq',
    math: 'math.h',
    iostream: '<iostream>',
    fstream: '<fstream>',
    ifstream: 'ifstream',
    ofstream: 'ofstream',
    std: 'std'
}

export const dataTypes = {
    int: 'int',
    double: 'double',
    bool: 'bool',
    long: 'long',
    void: 'void',
    char: 'char'
}

export const cycles = {
    for: 'for',
    while: 'while'
}

export const conditional = {
    if: 'if'
}

export const specialCharacter = {
    openBrace: '{',
    closedBrace: '}'
}

export const commonLibraries = {
    iostream: '<iostream>',
    fstream: '<fstream>',
    string: '<string.h>',
    stdio: '<stdio.h>',
    math: '<math.h>'
}