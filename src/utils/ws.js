import WebSocket from 'ws';

let wss = null;

export const initWS = (server) => {
    wss = new WebSocket.Server({server});
}


export const broadcast = (data) => {
    const string = JSON.stringify(data);
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(string);
        }
    });
};