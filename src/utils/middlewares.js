export const exceptionHandler = async (ctx, next) => {
  try {
    return await next();
  } catch (err) {
    ctx.response.body = { message: err.message || 'Unexpected error.' };
    ctx.response.status = err.status || 500;
  }
};

export const timingLogger = async (ctx, next) => {
  const start = Date.now();
  await next();
  console.log(`${ctx.method} ${ctx.url} => ${ctx.response.status}, ${Date.now() - start}ms`);
};

(() => {
  const originalLog = console.log;
  // Overwriting
  console.log = function () {
    var args = [].slice.call(arguments);
    originalLog.apply(console.log, [getCurrentDateString()].concat(args));
  };
  // Returns current timestamp
  function getCurrentDateString() {
    return `[${(new Date()).toISOString()}]:`;
  };
})();