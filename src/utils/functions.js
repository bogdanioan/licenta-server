export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
}