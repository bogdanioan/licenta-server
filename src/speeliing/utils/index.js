const levenshtein = require('js-levenshtein');

export function autoCorrectWord(dictionary, word) {
    if (dictionary.length === 0) {
        return [{ word}];
    }
    const results = dictionary.map(row => {
        return {
            word: row, rank: levenshtein(word, row)
        };
    });
    return results.sort((a, b) => { return a.rank - b.rank });

}
