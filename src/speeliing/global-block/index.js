import { autoCorrectWord } from "../utils";
import { cppKeywords, dataTypes, commonLibraries } from "../../utils";

export function autocorrectGlobalDataBlock(method) {
    method.globalVariables = [];
    method.lines.forEach(line => {
        if (line.words[0].startsWith('#')) {
            line.words[0] = cppKeywords.include;
            const rawLibrary = line.words.slice(1).join('');
            const resultedLibrary = autoCorrectWord(Object.values(commonLibraries), rawLibrary)[0].word;
            line.words = [line.words[0], resultedLibrary];
        } else {
            const result = autoCorrectWord(Object.values(cppKeywords), line.words[0]);
            line.words[0] = result[0].word;
            if (!dataTypes.hasOwnProperty(line.words[0])) {
                keywordsLineAutocorrect(line);
            } else {
                let restOfLine = line.words.slice(1).join('');
                if (restOfLine[restOfLine.length - 1] == ';') {
                    restOfLine = restOfLine.slice(0, restOfLine.length - 1);
                }
                method.globalVariables.push(...restOfLine.split(','));
            }
        }
    });
    return method;
}


function keywordsLineAutocorrect(line) {
    for (let index = 1; index < line.words.length; index++) {
        const result = autoCorrectWord(Object.values(cppKeywords), line.words[index]);
        line.words[index] = result[0].word;
    }
    line.words.push(';');
}