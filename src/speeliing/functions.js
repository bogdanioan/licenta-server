import { cppKeywords, dataTypes, specialCharacter, cycles, conditional } from '../utils'
import { autocorrectGlobalDataBlock } from './global-block';
import { autoCorrectWord } from './utils';


export function correctText(blocks) {
    const globalDataBlock = autocorrectGlobalDataBlock(blocks[0]);
    let methods = blocks.slice(1, blocks.length - 1);
    methods = methods.map(method => {
        return autocorrectMethodDefinition(method);
    });
    const mainFunction = autoCorrectMainFunctionDefinition(blocks[blocks.length - 1]);
    methods.push(mainFunction);
    //labels reprezinta toate variabilele globale si numele de functii
    const labels = globalDataBlock.globalVariables.map(v => ({ name: v, isFunction: false }));
    methods.forEach(m => {
        labels.push({ name: m.name, parameters: m.parameters, hasParameters: m.hasParameters, isFunction: true });
    })
    methods = methods.map(method => {
        return autocorrectInstructionBlocks(method, labels);
    })
    const text = finalText(globalDataBlock, methods);
    return { text, code: [globalDataBlock, ...methods] };
}

function autoCorrectMainFunctionDefinition(method) {
    eliminateUndesiredCharacters(method);
    method.lines[0].words[0] = cppKeywords.int;
    method.returnedType = cppKeywords.int;
    autocorrectMethodName(method);
    method.lines[0].words[1] = 'main';
    method.name = 'main';
    eliminateEmptyWords(method);
    const hasParameters = method.lines[0].words.length > 5;
    method.parameters = [];
    method.hasParameters = hasParameters;
    if (hasParameters) {
        method.lines[0].words = ['int', 'main', '(', 'int', 'argc', ',', 'char**', 'argv', ')', '{'];
        method.parameters = [
            {
                "type": "int",
                "name": "argc"
            },
            {
                "type": "char**",
                "name": "argv"
            }
        ];
        method.hasParameters = true;
    } else {
        method.lines[0].words = ['int', 'main', '(', ')', '{'];
        method.parameters = [];
        method.hasParameters = false;
    }
    return method;
}

function autocorrectMethodDefinition(method) {
    eliminateUndesiredCharacters(method);
    autocorrectReturnedType(method);
    autocorrectMethodName(method);
    eliminateEmptyWords(method);
    autoCorrectParameters(method);
    return method;
}

function eliminateEmptyWords(method) {
    for (let index = 2; index < method.lines[0].words.length; index++) {
        if (method.lines[0].words[index] === '') {
            method.lines[0].words.splice(index, 1);
        }
    }
}

function autoCorrectParameters(method) {
    const hasParameters = method.lines[0].words.length > 5;
    method.parameters = [];
    method.hasParameters = hasParameters;
    if (hasParameters) {
        fixParantheses(method);
        const parametersDefiniton = method.lines[0].words.slice(3, method.lines[0].words.length - 2);
        if (parametersDefiniton.length == 2) {
            // un singur parametru
            const type = autoCorrectWord(Object.values(dataTypes), parametersDefiniton[0])[0].word;
            method.parameters.push({ type, name: parametersDefiniton[1] });
            method.lines[0].words[3] = type;
        } else {
            const tmp = parametersDefiniton.join('@');
            const hasCommas = tmp.indexOf(',') > 0; //we assume if it recognised a comma it recognised all
            if (hasCommas) {
                const parameters = tmp.split(',');
                for (let index = 0; index < parameters.length; index++) {
                    const parameter = parameters[index].split('@');
                    const type = autoCorrectWord(Object.values(dataTypes), parameter[0])[0].word;
                    method.parameters.push({ type, name: parameter[1] });
                    method.lines[0].words[3 + 3 * index] = type;
                }
            } else {
                for (let index = 0; index < parametersDefiniton.length; index += 2) {
                    const type = autoCorrectWord(Object.values(dataTypes), parametersDefiniton[index])[0].word;
                    method.parameters.push({ type, name: parametersDefiniton[1] });
                    method.lines[0].words[3 + 3 * index] = type;
                    if (index !== parametersDefiniton.length - 2) {
                        method.lines[0].words.splice(5 + 3 * index, 0, ',');
                    }
                }
            }
        }


    }

}

function fixParantheses(method) {
    let posibleParameters = method.lines[0].words.slice(3);
    const hasClosedBrace = posibleParameters.join('').indexOf(')') > -1;
    if (hasClosedBrace) {
        const indexContainingBrace = posibleParameters.findIndex(w => w.indexOf(')') > -1);
        const containingWord = posibleParameters[indexContainingBrace];
        const indexBrace = containingWord.indexOf(')');

        if (indexBrace == 0 && containingWord.length == 1) {

            return;
        }
        if (indexBrace > -1) {
            const newWord = containingWord.slice(0, indexBrace);
            posibleParameters.splice(indexContainingBrace, 1, newWord);
            posibleParameters.splice(indexContainingBrace + 1, 0, ')');
        }
    } else {
        posibleParameters.splice(posibleParameters.length - 2, 0, ')');
    }
    method.lines[0].words.splice(3, method.lines.length - 3, ...posibleParameters);
}
function eliminateUndesiredCharacters(method) {
    method.lines[0].words = method.lines[0].words.map(w => {
        return w.replace(/[?@#$%]/g, '');
    })
}
function autocorrectMethodName(method) {
    const definition = method.lines[0].words.slice(1).join('');
    const isOperBrace = definition.indexOf('(') > -1;
    let methodName;
    if (isOperBrace) {
        const indexContainingBrace = method.lines[0].words.slice(1).findIndex(w => w.indexOf('(') > -1);
        const containingWord = method.lines[0].words[indexContainingBrace + 1];
        methodName = method.lines[0].words.slice(1, indexContainingBrace + 1).join('');
        const indexBrace = containingWord.indexOf('(');

        if (indexBrace > 0) {
            methodName = `${methodName}${containingWord.slice(0, indexBrace)}`;

            method.lines[0].words.splice(1, indexContainingBrace - 1, methodName);
            const newWord = containingWord.slice(indexBrace + 1);
            method.lines[0].words[2] = '(';
            method.lines[0].words.splice(3, 0, newWord);
        }
    } else {
        let hasParameters = false;
        for (let index = 2; index < method.lines[0].words.length; index++) {
            let result = autoCorrectWord(Object.values(dataTypes), method.lines[0].words[index])[0];
            let parameterMark = result.rank < 3;
            if (parameterMark) {
                methodName = method.lines[0].words.slice(1, index).join('');
                method.lines[0].words.splice(1, index - 1, methodName);
                method.lines[0].words.splice(index, 0, '(');
                method.lines[0].words[index + 1] = result.word;
                hasParameters = true;
                break;
            }
        }
        if (!hasParameters) {
            const hasClosedBrace = definition.indexOf(')') > -1;
            if (hasClosedBrace) {
                const indexContainingBrace = method.lines[0].words.slice(1).findIndex(w => w.indexOf(')') > -1);
                const containingWord = method.lines[0].words[indexContainingBrace + 1];
                methodName = method.lines[0].words.slice(1, indexContainingBrace + 1).join('');
                const indexBrace = containingWord.indexOf(')');

                if (indexBrace > 0) {
                    methodName = `${methodName}${containingWord.slice(0, indexBrace)}`;

                    method.lines[0].words.splice(1, indexContainingBrace - 1, methodName);
                    method.lines[0].words[2] = '(';
                    method.lines[0].words.splice(3, 0, ')');
                }
            } else {
                methodName = method.lines[0].words.slice(1, method.lines[0].words.length - 1).join('');
                method.lines[0].words.splice(1, method.lines[0].words.length - 2, methodName);
                method.lines[0].words.splice(2, 0, '(');
                method.lines[0].words.splice(3, 0, ')');
            }
        }
    }
    method.name = methodName;
}

function autocorrectReturnedType(method) {
    let returnType = method.lines[0].words[0];
    if (returnType.length == 1) {
        //too far => delete it
        method.lines[0].words.splice(0, 1);
    }
    returnType = method.lines[0].words[0];
    let result = autoCorrectWord(Object.values(dataTypes), returnType);
    method.lines[0].words[0] = result[0].word;
    method.returnedType = result[0].word;
    return method.lines[0].words[0];
}

function finalText(globalDataBlock, methods) {
    const lineSeparator = '\n';
    const wordSeparator = ' ';
    let newText = '';
    methods = methods.map(method => {
        return indentCode(method);
    })
    globalDataBlock.lines.forEach(line => {
        line.words.forEach(word => {
            newText = `${newText}${wordSeparator}${word}`;
        });
        newText = `${newText}${lineSeparator}`;
    });
    newText = `${newText}${lineSeparator}`;
    methods.forEach(method => {
        method.lines.forEach(line => {
            let tabs = '';
            for (let index = 0; index < line.numberOfTabs; index++) {
                tabs = `${tabs}\t`;
            }
            newText = `${newText}${tabs}`
            line.words.forEach(word => {
                newText = `${newText}${wordSeparator}${word}`;
            });
            newText = `${newText}${lineSeparator}`;
        });
        newText = `${newText}${lineSeparator}`;
    });
    return newText;
}

function indentCode(method) {
    const lines = method.lines;
    lines[0].numberOfTabs = 0;
    for (let index = 1; index < lines.length; index++) {
        lines[index].numberOfTabs = lines[index].indent;
    }
    return method;
}

function autocorrectForInitalise(instructionAsString, localVariables) {
    const result = [];
    const isDeclaration = instructionAsString.indexOf('int') == 0;
    if (isDeclaration) {
        result.push('int');
        let instruction = instructionAsString.slice(3);
        const indexOfEqual = instruction.indexOf('=');
        if (indexOfEqual > -1) {
            let counterName = instruction.slice(0, indexOfEqual);
            result.push(counterName);
            result.push('=');
            result.push(instruction.slice(indexOfEqual + 1));
        } else {
            result.push(instruction);
        }
    } else {
        const indexOfEqual = instructionAsString.indexOf('=');
        if (indexOfEqual > -1) {
            let counterName = instructionAsString.slice(0, indexOfEqual);
            let candidateVariables = localVariables.filter(v => v.type === cppKeywords.int)
                .map(v => v.name);
            counterName = autoCorrectWord(candidateVariables, counterName)[0].word;
            result.push(counterName);
            result.push('=');
            result.push(instructionAsString.slice(indexOfEqual + 1));
        } else {
            result.push(instructionAsString);
        }
    }
    return result;
}

function autocorrectOpenBracedInstruction(line, localVariables) {
    const type = line.words[0];
    if (type === cppKeywords.for) {
        let tmp = line.words.slice(1, line.words.length - 1).join('@');
        tmp = tmp.replace(/[\(\)]/g, '');
        tmp = tmp.replace(/[;]/g, '@;@');
        tmp = tmp.split('@').filter(s => s !== '');

        line.words.splice(1, line.words.length - 1);
        line.words.push('(');

        const indexOfFirstSemicolon = tmp.findIndex(w => w === ';');
        let dataTypeLookup = autoCorrectWord([cppKeywords.int], tmp[0])[0];
        if (dataTypeLookup.rank < 3) {
            const correctedInstruction = autocorrectForInitalise([dataTypeLookup.word, ...tmp.slice(1, indexOfFirstSemicolon)].join(''), localVariables);
            line.words.push(...correctedInstruction);
        } else {
            line.words.push(tmp.slice(0, indexOfFirstSemicolon).join(''));
        }
        line.words.push(';');
        const indexOfSecondSemicolon = tmp.slice(indexOfFirstSemicolon + 1).findIndex(w => w === ';') + indexOfFirstSemicolon + 1;
        line.words.push(tmp.slice(indexOfFirstSemicolon + 1, indexOfSecondSemicolon).join(''));
        line.words.push(';');
        line.words.push(tmp.slice(indexOfSecondSemicolon + 1).join(''));
        line.words.push(')');
        line.words.push('{');
    }
    if (type === cppKeywords.while) {
        let tmp = line.words.slice(1, line.words.length - 1).join('@');
        tmp = tmp.replace(/[\(\)]/g, '');
        tmp = tmp.split('@').filter(s => s !== '');
        line.words.splice(1, line.words.length - 1);
        line.words.push('(');
        line.words.push(...tmp);
        line.words.push(')');
        line.words.push('{');
    }
    if (type === cppKeywords.if) {
        let tmp = line.words.slice(1, line.words.length - 1).join('@');
        tmp = tmp.replace(/[\(\)]/g, '');
        tmp = tmp.split('@').filter(s => s !== '');
        line.words.splice(1, line.words.length - 1);
        line.words.push('(');
        line.words.push(...tmp);
        line.words.push(')');
        line.words.push('{');
    }
}

function autocorrectDeclaration(localVariables, line) {
    let numberOfWords = line.words.length - 1;
    let lastWord = line.words[numberOfWords];
    let firstWord = line.words[0];
    const hasSemicolon = lastWord.indexOf(';') > -1;
    if (hasSemicolon) {
        lastWord = lastWord.replace(/[;]/g, '');
        line.words[numberOfWords] = lastWord;
    }
    const localVariablesDefinition = line.words.slice(1);
    line.words.splice(1, line.words.length - 1);
    let tmp = localVariablesDefinition.join('');
    const hasCommas = tmp.indexOf(',') > 0; //we assume if it recognised a comma it recognised all
    if (hasCommas) {
        const variables = tmp.split(',');
        for (let i = 0; i < variables.length; i++) {
            if (variables[i].indexOf('=') > -1) {
                //to do la master
            }

            localVariables.push({ type: firstWord, name: variables[i] });
            line.words.push(variables[i]);
            if (i < variables.length - 1) {
                line.words.push(',');
            }
        }
    } else {
        for (let i = 0; i < localVariablesDefinition.length; i++) {
            if (localVariablesDefinition[i].indexOf('=') > -1) {
                line.words[i + 1] = '=';
                continue;
            }
            if (line.words[i] === '=' && firstWord === dataTypes.bool) {
                const value = autoCorrectWord([cppKeywords.true, cppKeywords.false], localVariablesDefinition[i])[0].word;
                line.words.push(value);
            } else {
                if (line.words[i] === '=') {
                    line.words.push(localVariablesDefinition[i]);
                } else {
                    localVariables.push({ type: firstWord, name: localVariablesDefinition[i] });
                    line.words.push(localVariablesDefinition[i]);
                }
            }
            if (i < localVariablesDefinition.length - 1) {
                line.words.push(',');
            }
        }
    }
    line.words.push(';');
}

function autocorrectInstructionBlocks(method, labels) {
    method.localVariables = [];
    const methodParameters = method.parameters.map(p => p.name);
    const globalVariables = labels.map(l => l.name);
    const lines = method.lines;
    const numberOfLines = lines.length;
    for (let index = 1; index < numberOfLines; index++) {
        const numberOfWords = lines[index].words.length;
        const firstWord = lines[index].words[0];
        const labelsDictionary = [...globalVariables, ...methodParameters, ...method.localVariables.map(v => v.name)];
        if (lines[index].words[numberOfWords - 1] === specialCharacter.closedBrace) {
            continue;
        }

        if (lines[index].words[numberOfWords - 1] === specialCharacter.openBrace) {
            const result = autoCorrectWord([...Object.values(conditional), ...Object.values(cycles)], firstWord);
            method.lines[index].words[0] = result[0].word;
            autocorrectOpenBracedInstruction(method.lines[index], method.localVariables);
        } else {
            // orice altceva
            const keywordLookUp = autoCorrectWord(Object.values(cppKeywords), firstWord)[0];
            const labelLookup = autoCorrectWord(labelsDictionary, firstWord)[0];
            if (keywordLookUp.rank < labelLookup.rank) {
                method.lines[index].words[0] = keywordLookUp.word;
                //we have  break / continue

                if (keywordLookUp.word === cppKeywords.break || keywordLookUp.word === cppKeywords.continue) {
                    method.lines[index].words = [`${keywordLookUp.word};`];
                    continue;
                }


                if (keywordLookUp.word === cppKeywords.return) {
                    method.lines[index].words[0] = cppKeywords.return;
                    if (method.name === 'main') {
                        method.lines[index].words = [cppKeywords.return, '0', ';'];
                        continue;
                    }
                    let rest = method.lines[index].words.slice(1);
                    if (rest.length === 1) {
                        rest = rest[0];
                        rest.replace(/[;]/g, '');
                        let candidateVariables = method.localVariables.filter(v => v.type === method.returnedType)
                            .map(v => v.name);
                        const variable = autoCorrectWord(candidateVariables, rest)[0];
                        if (variable.rank < 3) {
                            method.lines[index].words[1] = variable.word;
                            method.lines[index].words.push(';');
                        } else {
                            if (method.returnedType === cppKeywords.bool) {
                                method.lines[index].words[1] = autoCorrectWord([cppKeywords.true, cppKeywords.false], rest)[0].word;
                                method.lines[index].words.push(';');
                            }
                        }
                    }
                    continue;
                }

                if (dataTypes[keywordLookUp.word] !== undefined) {
                    autocorrectDeclaration(method.localVariables, method.lines[index]);
                }
            } else {
                method.lines[index].words[0] = labelLookup.word;
                method.lines[index].words[numberOfWords - 1] = method.lines[index].words[numberOfWords - 1].replace(/[;]/g, ';');
                method.lines[index].words.push(';');
            }
        }
    }
    return method;
}