import Router from 'koa-router';
import { broadcast } from '../utils';
import { correctText } from '../speeliing';
import { convert, buildBlocksLines } from '../handwritting';
import { saveCppFile, saveImageFile, saveJsonFile, saveDirectory, getImage, getDetails, saveGoogleCppFile } from '../file-management/functions';
import { proccesAndSaveImage} from '../image-proccessing';

export const router = new Router();
router.post('/', async (ctx) => {
    const reference = Date.now();
    const response = ctx.response;
    const body = ctx.request.body;
    broadcast({ fileName: body.fileName, status: 'Saving files to server ...' });
    saveDirectory(reference);
    const filePath = saveImageFile(body.file, reference);
    broadcast({ fileName: body.fileName, status: 'Image proccessing activated ...' });
    const scaledAndBinarizedFilePath = proccesAndSaveImage(filePath);
    broadcast({ fileName: body.fileName, status: 'Extracting the code ...' });
    const result = await convert(scaledAndBinarizedFilePath);
    broadcast({ fileName: body.fileName, code: result.text });
    saveGoogleCppFile(result.text, filePath);
    broadcast({ fileName: body.fileName, status: 'The server is sweating ...' });
    const blocks = buildBlocksLines(result);
    const finalResult = correctText(blocks);
    saveJsonFile(finalResult.code, filePath);
    saveCppFile(finalResult.text, filePath);
    response.body = { text: finalResult.text, reference };
    response.status = 200;
});

router.get('/', async (ctx) => {
    const params = ctx.querystring.split('=');
    const param = params[0];
    const value = params[1];
    switch (param) {
        case 'img': {
            const img = getImage(value);
            ctx.response.body = { img };
            ctx.response.status = 200;
            break;
        }
        case 'id': {
            const details = getDetails(value);
            ctx.response.body = details;
            ctx.response.status = 200;
            break;
        }
        default: {
            ctx.response.status = 404;
            break;
        }
    }

})
