import { constants } from '../utils';
const fs = require('fs');

export function getImage(name) {
    const filePath = `${constants.imagesBasePath}/${name}/${name}.jpeg`;
    return fs.readFileSync(filePath, 'base64');
}

export function saveGoogleCppFile(text, filePath) {
    const suffix = '-google.cpp';
    const index = filePath.lastIndexOf('.');
    const newFilePath = `${filePath.substring(0, index)}${suffix}`;
    fs.writeFile(newFilePath, text, function (err) {
        if (err) {
            return console.log(err);
        }

        console.log(`The file saved successfully to ${newFilePath}`);
    });
    return filePath;
}

export function getDetails(name) {
    const filePath = `${constants.imagesBasePath}/${name}/${name}.cpp`;
    const filePathGoogle = `${constants.imagesBasePath}/${name}/${name}-google.cpp`;
    const text = fs.readFileSync(filePath, 'utf8');
    const textGoogle = fs.readFileSync(filePathGoogle, 'utf8');
    return { text, textGoogle };
}

export function saveCppFile(text, filePath) {
    const suffix = '.cpp';
    const index = filePath.lastIndexOf('.');
    const newFilePath = `${filePath.substring(0, index)}${suffix}`;
    fs.writeFile(newFilePath, text, function (err) {
        if (err) {
            return console.log(err);
        }

        console.log(`The file saved successfully to ${newFilePath}`);
    });
    return filePath;
}

export function saveJsonFile(object, filePath) {
    const suffix = '.json';
    const index = filePath.lastIndexOf('.');
    const newFilePath = `${filePath.substring(0, index)}${suffix}`;
    fs.writeFile(newFilePath, JSON.stringify(object, null, 2), function (err) {
        if (err) {
            return console.log(err);
        }

        console.log(`The file saved successfully to ${newFilePath}`);
    });
    return filePath;
}

export function saveImageFile(file, fileName) {

    const ext = file.substring(file.indexOf("/") + 1, file.indexOf(";base64"));
    const fileType = file.substring("data:".length, file.indexOf("/"));
    //Forming regex to extract base64 data of file.
    const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
    //Extract base64 data.
    const base64Data = file.replace(regex, "");
    // const filePath = `${constants.imagesBasePath}/${fileName}/${fileName}.${ext}`;
    const filePath = `${constants.imagesBasePath}/${fileName}/${fileName}.jpeg`;
    try {
        fs.writeFileSync(filePath, base64Data, { encoding: 'base64' });
    } catch (err) {
        console.log(err);
    }
    console.log(`Image file saved successfully to ${filePath}`);
    return filePath;
}

export function saveDirectory(directoryName) {
    const dir = `${constants.imagesBasePath}/${directoryName}`;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
        console.log(`Directory ${dir} created successfully`);
    } else {
        console.log(`Directory ${dir} already exists`);
    }

}