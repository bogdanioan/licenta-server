import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
// import jwt from 'koa-jwt';
import cors from 'koa-cors';
import { exceptionHandler, timingLogger } from "./utils";
import { router as fileRouter } from './api';
// import WebSocket from 'ws';
import { initWS } from './utils/ws';

const app = new Koa();
const server = require('http').createServer(app.callback());
initWS(server);
app.use(exceptionHandler);
app.use(timingLogger);
app.use(bodyParser());
app.use(cors());
// public
const prefix = '/api';
const publicApiRouter = new Router({ prefix });
publicApiRouter
  .use('/file', fileRouter.routes());
app
  .use(publicApiRouter.routes())
  .use(publicApiRouter.allowedMethods());
// app.use(jwt(jwtConfig));

server.listen(3000);
console.log("Listening on port 3000");
// setTimeout(async () => await start(), 3000);