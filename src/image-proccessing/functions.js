const sharp = require('sharp');
export function proccesAndSaveImage(filePath) {
    const suffix = 'scaled';
    const index = filePath.lastIndexOf('.');
    const newFilePath = `${filePath.substring(0, index)}-${suffix}${filePath.substring(index)}`;
    sharp(filePath)
        .resize(1024)
        .toFile(newFilePath)
        .then(_ => {
            console.log(`Image proccesed successfully to ${newFilePath}`)
        })
        .catch(err => { console.log(err) });
    return newFilePath;
}
