import { delimitBlocks } from './blocks-delimiter';
import { delimitIndent } from './indent-delimiter';

export async function convert(fileName) {
    const vision = require('@google-cloud/vision');
    const client = new vision.ImageAnnotatorClient();
    const [result] = await client.documentTextDetection(fileName);
    const fullTextAnnotation = result.fullTextAnnotation;
    return fullTextAnnotation;
}

function calculateMaxY(symbol) {
    let sum = 0;
    let count = 0;
    symbol.boundingBox.vertices.map(v => {
        sum = sum + v.y;
        count = count + 1;
    });
    return sum / count;
}

function calculateMinX(symbol) {
    return Math.min.apply(null, symbol.boundingBox.vertices.map(v => v.x));
}
function getLines(text, separatorLine) {
    const separatorWords = ' ';
    let fullText = text.text;
    fullText = fullText.replace(/[\}\{]/g, '');
    const lines = fullText.split(separatorLine).filter(line => !line.startsWith('/'));
    const resultedLines = [];
    const indexOfFirstWord = [0];
    for (const line of lines) {
        const words = getWords(line, separatorWords);
        resultedLines.push({ words });
        indexOfFirstWord.push(indexOfFirstWord[indexOfFirstWord.length - 1] + words.length);
    }
    indexOfFirstWord.pop();
    let indexOfCurrentLine = 0;
    let numberOfWords = 0;
    text.pages[0].blocks.forEach(block => {
        block.paragraphs.forEach(paragraph => {
            paragraph.words.forEach(word => {
                word.symbols.forEach(symbol => {
                    if (indexOfFirstWord[indexOfCurrentLine] === numberOfWords) {
                        const minX = calculateMinX(symbol);
                        const maxY = calculateMaxY(symbol);
                        resultedLines[indexOfCurrentLine].startingX = minX;
                        resultedLines[indexOfCurrentLine].startingY = maxY;
                        indexOfCurrentLine++;
                    };
                    if (symbol.property && !!symbol.property.detectedBreak) {
                        numberOfWords++;
                    }
                });
            })
        });
    });
    const lastLine = resultedLines[resultedLines.length - 1];
    if (lastLine.words.length == 1 && lastLine.words[0] == '')
        resultedLines.pop();
    return resultedLines.sort((a, b) => a.startingY - b.startingY);
}

function getWords(line, separatorWord) {
    return line.split(separatorWord);
}

export function buildBlocksLines(textAnnotation) {
    const lines = getLines(textAnnotation, '\n');
    let blocks = delimitBlocks(lines);
    blocks = delimitIndent(blocks);
    return blocks;
}

