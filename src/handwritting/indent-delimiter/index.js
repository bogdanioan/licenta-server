export function delimitIndent(blocks) {
    const methods = blocks.slice(1).map(method => {
        return insertBraces(method);
    });
    return [blocks[0], ...methods];
}

function insertBraces(method) {
    const lines = method.lines;
    calculateXTreshold(method);
    establishLineIndents(method);
    let numberOfLines = lines.length;
    for (let index = 1; index < numberOfLines; index++) {
        if (lines[index].indent > lines[index - 1].indent) {
            method.lines[index - 1].words.push('{');
        }

        if (lines[index].indent < lines[index - 1].indent) {
            let brackets = [];
            for (let i = lines[index - 1].indent - 1; i >= lines[index].indent; i--) {
                let line = { words: ['}'], indent: i };
                brackets.push(line);
            }
            method.lines.splice(index, 0, ...brackets);
            numberOfLines += brackets.length;
            index += brackets.length;
        }
    }
    let line = { words: ['}'], indent: 0 };
    method.lines.push(line);
    return method;
}

function establishLineIndents(method) {
    method.lines[0].indent = 0;
    let methodDiff = method.lines[method.lines.length - 1].startingX - method.lines[0].startingX;
    methodDiff = methodDiff - method.indentUnit;
    if (methodDiff < 0) {
        methodDiff = 0;
    }
    const startingX = method.lines[0].startingX;
    method.lines[0].startingX = 0;

    for (let index = 1; index < method.lines.length; index++) {
        method.lines[index].startingX =
            method.lines[index].startingX -
            getLineOffset(index, method.lines.length,
                methodDiff, startingX);
        let segmentStart = Math.floor(method.lines[index].startingX / method.indentUnit);
        let start = segmentStart * method.indentUnit;
        let end = (segmentStart + 1) * method.indentUnit;
        if (method.lines[index].startingX < (end + start) / 2) {
            method.lines[index].indent = segmentStart;
        } else {
            method.lines[index].indent = segmentStart + 1;
        }

        if (method.lines[index].indent - method.lines[index - 1].indent > 1) {
            method.lines[index].indent = method.lines[index - 1].indent + 1;
        }
        let diff = method.lines[index - 1].startingX - method.lines[index].startingX;
        if (diff < 0) {
            continue;
        }

        segmentStart = Math.floor(diff / method.indentUnit);
        start = segmentStart * method.indentUnit;
        end = (segmentStart + 1) * method.indentUnit;
        if (diff < (end - start) / 2) {
            method.lines[index].indent = Math.round((method.lines[index - 1].indent - segmentStart + method.lines[index].indent) / 2);
        } else {
            method.lines[index].indent = Math.round((method.lines[index - 1].indent - segmentStart - 1 + method.lines[index].indent) / 2);
        }
    }
}

function getLineOffset(lineIndex, lineNumber, methodDiff, overhead) {
    return methodDiff * lineIndex / lineNumber + overhead;
}

function calculateXTreshold(method) {
    let lines = method.lines;
    let sum = 0;
    let tUp = 0;
    let counterUp = 0;
    for (let i = 1; i < lines.length; i++) {
        let diff = lines[i].startingX - lines[i - 1].startingX;
        if (diff < 0) {
            continue;
        }
        tUp = tUp + diff;
        counterUp = counterUp + 1;
    }
    tUp = tUp / counterUp;
    counterUp = 0;
    for (let i = 1; i < lines.length; i++) {
        let diff = lines[i].startingX - lines[i - 1].startingX;
        if (diff < tUp) {
            continue;
        }
    
        sum = sum + diff;
        counterUp = counterUp + 1;
    }
    sum = sum / counterUp;
    method.indentUnit = sum;
}