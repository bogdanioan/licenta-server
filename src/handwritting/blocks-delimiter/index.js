export function delimitBlocks(lines) {
    const yTreshold = calculateYTreshold(lines);
    const blocks = [{ lines: [lines[0]] }];
    const restOfLines = lines.slice(1);
    for (const line of restOfLines) {
        const lastBlock = blocks[blocks.length - 1];
        const lastBlockY = lastBlock.lines[lastBlock.lines.length - 1].startingY;
        if (line.startingY - lastBlockY < yTreshold) {
            blocks[blocks.length - 1].lines.push(line);
        } else {
            blocks.push({ lines: [line] });
        }

    }
    return blocks;
}

function calculateYTreshold(lines) {
    return (lines[lines.length - 1].startingY - lines[0].startingY) / (lines.length - 1);
}